# MyavAsm. Транслятор и модель

- P33081, Кравцова Кристина Владимировна.
- `asm | acc | neum | hw | tick | struct | trap | mem | pstr | prob2 | spi`
- Без усложнения

## Язык программирования

### Синтаксис

**Форма Бэкуса-Наура:**

```ebnf
<программа> ::= <строка_программы> | <строка_программы> <программа>
<строка_программы> ::= <адрес> | [<метка>] <адресная команда> <операнд> | 
[<метка>] <безадресная команда> | [<метка>] <метка константы> <константа> | <пустая строка> 

<метка> ::= <слово>
<адресная команда> = add | load | store | ... | sub | jmp | (см. систему команд)
<безадресная команда> ::= cla | di | ei | ... | hlt
<операнд> ::= <число> | <метка>
<константа> ::= <число> | <число> '<слово>'
<слово> ::= <символ> | <слово> <символ>
<число> ::= <цифра> | <число> <цифра>
<цифра> ::= 0| 1 | 2 | .. | 8 | 9
<символ> ::= a | b | c | ... | z | A | B | C | ... | Z | <цифра>
```

**Пояснение:**

Каждая непустая строка программы это одно из нижеперечисленных:

* **адресная команда**
    * может иметь метку в начале
    * указывается название команды и адрес операнда через пробел
* **безадресная команда**
    * может иметь метку в начале
    * указывается только название команды
* **константа**
    * может иметь метку в начале
    * указывается метка константы `word:` и константа
    * константа может быть 16-битным знаковым числом
    * константа может быть строкой: указывается длина строки и строка через пробел
* **адрес**
    * указывается специальное слово `org` и адрес в десятичном формате

Пример программы, вычисляющей С = A + B

```asm
org 5
A: word: 10
B: word: 15
C: word: 0

start: cla
load A
add B
store C
hlt
```

**Семантика**

- Видимость данных -- глобальная
- Поддерживаются целочисленные литералы, находящиеся в диапазоне от $`-2^{31}`$ до $`2^{31}-1`$
- Поддерживаются строковые литералы, символы стоки необходимо заключить в кавычки, перед строкой через запятую необходимо указать длину
- Код выполняется последовательно

- Программа обязательно должна включать метку `start:`, указывающую на 1-ю выполняемую интсрукцию. Эта метка не может
  указывать на константу.
- Название метки не должно совпадать с названием команды и не может начинаться с цифры.
- Метки находятся на одной строке с командами, операнды находятся на одной строке с командами.
- Пустые строки игнорируются, количество пробелов в начале и конце строки не важно.
- Любой текст, расположенный в конце строки после символа `';'` трактуется как комментарий.

Память выделяется статически, при запуске модели.

## Организация памяти

* Память команд и данныx --- общая
* Размер машинного слова --- `32` бит
* Память содержит `2^11` ячеек
* Ячейка с адресом `0` зарезервирована под вектор прерывания устройства ввода
* Адрес `2045` является указателем стека при старте процессора. Стек растет вверх.
* Ячейка с адресом `2046` маппится на устройство ввода
* Ячейка с адресом `2047` маппится на устройство вывода


* Поддерживаются следующие **виды адресаций**:
    * **Прямая**: в качестве аргумента команды передается адрес ячейки, значение в которой будет использовано как
      операнд.
      Например, если `mem[30] = 25`, то команда `add 30` обозначает, что к значению в аккумуляторе добавится число 25.

    * **Косвенная**: в качестве аргумента команды передается адрес, по которому лежит адрес операнда.
      Например, если `mem[30] = 25`, `mem[33] = 30`, то команда `add (33)` также обозначает, что к аккумулятору
      добавится значение 25.


* Существует несколько **регистров**:
    * Аккумулятор (AC): в него записываются результаты всех операций
    * Счетчик команд (IP): хранит адрес следующей выполняемой команды
    * Указатель стека (SP): при вызове прерывания текущее состояние счетчика команд сохраняется на стеке
    * Регистр состояния (PS): хранит маркер того, что наступило прерывание

      ```
        | ie/id | ir | N | Z | C |
         4        3    2   1   0
      ```
        * 0-й бит хранит значение флага C
        * 1-й бит хранит значение флага Z
        * 2-й бит хранит значение флага N
        * 3-й бит содержит 1, если поступил запрос прерывания, и 0 иначе
        * 4-й бит содержит 1, если прерывания разрешены (interrupts enabled) и 0, если запрещены (interrupts disabled)


* Регистр данных (DR): хранит данные для записи в память и считывания из памяти
* Регистр адреса (AR): хранит адрес последней ячейки в памяти, к которой было обращение

## Система команд

Особенности процессора:

- Машинное слово -- `32` бита, знаковое.
- В качестве аргументов команды принимают `11` битные беззнаковые адреса

Каждая команда выполняется в несколько циклов:

1. Цикл выборки команды: по адресу из счетчика команд из памяти достается команда

- `IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR`

2. Цикл выборки операнда (для адресных команд): в регистр данных помещается адрес операнда, регистр данных передавется в
   регистр адреса, из памяти в регистр данных записывается значение операнда

- `CR[addr] -> DR, DR -> AR, mem[AR] -> DR`

3. Цикл исполнения: совершаются действия, необходимые для выполнения команды. Результаты вычисления записываются в
   аккумулятор
4. Цикл прерывания: проверяется, не произошел ли запрос на прерывание

### Набор инструкций

| Язык  | Адресная | Ветвление | Количество тактов<br/>(без выборки адреса) | Описание                                                                                       |
|:------|:---------|-----------|:-------------------------------------------|:-----------------------------------------------------------------------------------------------|
| load  | +        | -         | 3                                          | загрузить значение из заданной ячейки                                                          |
| store | +        | -         | 3                                          | загрузить значение в заданную ячейку                                                           |
| add   | +        | -         | 3                                          | добавить значение из заданной ячейки к аккумулятору                                            |
| sub   | +        | -         | 3                                          | вычесть значение из заданной ячейки из аккумулятора                                            |
| jmp   | +        | +         | 3                                          | перейти в заданную ячейку                                                                      |
| cmp   | +        | -         | 3                                          | выставить флаги как результат вычитания заданной ячейки из аккумулятора, сохранить аккумулятор |
| jmn   | +        | +         | 3                                          | перейти в заданную ячейку если N = 1                                                           |
| jmnn  | +        | +         | 3                                          | перейти в заданную ячейку если N = 0                                                           |
| jmc   | +        | +         | 3                                          | перейти в заданную ячейку если C = 1                                                           |
| jmnc  | +        | +         | 3                                          | перейти в заданную ячейку если C = 0                                                           |
| jmz   | +        | +         | 3                                          | перейти в заданную ячейку если Z = 1                                                           |
| jmnz  | +        | +         | 3                                          | перейти в заданную ячейку если Z = 0                                                           |
| asl   | -        | -         | 2                                          | сдвинуть значение в аккумуляторе влево, AC[15] -> C                                            |
| asr   | -        | -         | 2                                          | сдвинуть значение в аккумуляторе вправо, AC[0] -> C                                            |
| dec   | -        | -         | 2                                          | уменьшить значение в аккумуляторе на 1                                                         |
| inc   | -        | -         | 2                                          | увеличить значение в аккумуляторе на 1                                                         |
| cla   | -        | -         | 2                                          | очистить аккумулятор (записать в него 0)                                                       |
| hlt   | -        | -         | 2                                          | остановить работу программы                                                                    |
| iret  | -        | -         | 2                                          | возрат из прерывания                                                                           |
| push  | -        | -         | 2                                          | положить значение из аккумулятора на стек                                                      |
| pop   | -        | -         | 2                                          | достать значение с вершины стека и записать в аккумулятор                                      |
| di    | -        | -         | 2                                          | запретить прерывания                                                                           |
| ei    | -        | -         | 2                                          | разрешить прерывания                                                                           |
| nop   | -        | -         | 1                                          | отсутствие операции                                                                            |


### Кодирование инструкций

- Машинный код сереализуется в список JSON.
- Один элемент списка, одна инструкция или константа.

Пример сереализованной команды `load (1)` и константы 20:

```json
[
  {
    "index": 10,
    "opcode": "load",
    "operand": 1,
    "value": 0,
    "address": True
  },
  {
    "index": 2,
    "value": 20,
    "opcode" : "nop"
  }
]
```

где:

- `index` -- адрес объекта в памяти;
- `opcode` -- код оператора, идентификатор команды; У констант для упрощения всегда "nop"
- `operand` -- аргумент команды, адрес ячейки над которой совершается операция. Отсуствует у безадресных команд и
  констант;
- `address` -- логическое поле, имеющее значение True, если адресация косвенная, и False, если прямая.
- `value` -- значение ячейке с адресом index, если она будет интерпретирована как данные. Так как команды сериализуются
  в высокоуровневую структуру, было принято решение установить у команд это значение в 0.

## Транслятор

Интерфейс командной строки: `mv_translator.py <input_file> <target_file>`

Реализовано в модуле: [mv_translator](./mv_translator.py)

Этапы трансляции (функция `translate`):

1. Выделение меток из кода, проверка их корректности (не совпадают с названиями команд, отсуствуют дубликаты)
2. Парсинг кода построчно, определение типа команды (адресная, безадресная, константа)
3. Генерация машинного кода в зависимости от типа команды

Правила генерации машинного кода:

- Метки не сохраняются в машинном коде. Метки, использованные в качестве операнда, преобразуются к адресам команд

## Модель процессора

Интерфейс командной строки: `mv_mashine.py <machine_code_file> <input_file>`

Реализовано в модуле: [mv_mashine](./mv_machine.py).

### DataPath

Реализован в классе `DataPath`.

![Data Path](./processor.png)

`data_memory` -- однопортовая память, поэтому либо читаем, либо пишем.
`registers`
Сигналы (реализованы в виде методов класса):
В

- `set_reg` -- защёлкнуть выбранное значение в регистре с указанным именем
- `rd` --- считать данные из `mem[AR]` в регистр `DR`
- `wr` --- записать данные из регистра `DR` в `mem[AR]`

В виде отдельного класса реализовано арифметико-логическое устройство (АЛУ)

- в данном классе реализован метод `calc`, принимающий аргументы с одного или двух входов и совершающий над ними
  арифметико-логическую операцию
- в результате выполнения операций устанавливаются следующие флаги
    - `Z` -- значение в аккумуляторе равно 0
    - `N` -- значение в аккумуляторе отрицательно
    - `C` -- произошло переполнение (перенос из 16-го бита)

### ControlUnit

Реализован в классе `ControlUnit`.

![Control Unit](./control_unit.png)

- Метод `decode_and_execute_instruction` моделирует выполнение полного цикла инструкции (цикл выборки инструкции,
  операнда, исполнения)
- После завершения цикла исполнения проверяется, не произошел ли запрос прерывания, и разрешены ли прерывания. Если оба
  условия верны, то вызывается метод `process_interrupt`
- В рамках реализованной модели на python существуют счетчик количества инструкций только для наложения ограничения на
  кол-во шагов моделирования

Особенности работы модели:

- Цикл симуляции осуществляется в функции `simulation`.
- Шаг моделирования соответствует одному такту процессора с выводом состояния в журнал.
- Для журнала состояний процессора используется стандартный модуль `logging`.
- Количество инструкций для моделирования лимитировано.
- Остановка моделирования осуществляется при:
    - превышении лимита количества выполняемых инструкций;
    - исключении `Incorrect unary operation` или `Incorrect binary operation`-- если в ALU поданны некорректные бинврные или унарные операции
    - если выполнена инструкция `hlt`.

- обработка прерываний осуществляется в методе `process_interrupt`
    - на стек сохраняются текущие значения счетчика команд (IP), и регистра состояния (PS)
    - в IP записывается адрес из вектора прерываний (хранится в ячейке 0)
    - выполняются все команды для обработки прерывания. При выполнении команды  `iret` происходит возврат в основную
      программу
    - из стека достаются значения IP и PS и присваиваются соответствующим регистрам

Проверка наличия запроса прерывания осуществляется после завершения цикла выполнения очередной команды.

- Вложенные прерывания возможны, программист должен управлять запретом и разрешением прерываний самостоятельно при
  помощи команд:
    - ei (enable interrupt) --- разрешить прерывания
    - di (disable interrupt) --- запретить прерывания
- Все регистры кроме PS и IP программист должен самостоятельно сохранять на стек в методе-обработчике прерываний.

## Тестирование

Реализованные програмы

1. [hello world](examples/myasm/hello.myasm): вывести на экран строку `'Hello World!'`
2. [cat](examples/myasm/cat.myasm): программа `cat`, повторяем ввод на выводе.
3. [hello_username](examples/myasm/hello_username.myasm) -- программа `hello_username`: запросить у пользователя его
   имя, считать его, вывести на экран приветствие
4. [prob2](examples/myasm/prob2.myasm): найти сумму всех четных чисел Фибоначчи, не превышающих `4 000 000`.

Интеграционные тесты реализованы тут [mv_integration_test](./mv_integration_test.py):

- через golden tests, конфигурация которых лежит в папке [golden](./golden)

CI:

``` yaml
lab3-example:
  stage: test
  image:
    name: ryukzak/python-tools
    entrypoint: [""]
  script:
    - cd src/brainfuck
    - poetry install
    - coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t coverage report
    - ruff format --check .
```

где:

- `ryukzak/python-tools` -- docker образ, который содержит все необходимые для проверки утилиты.
  Подробнее: [Dockerfile](/src/brailfuck/Dockerfile)
- `poetry` -- управления зависимостями для языка программирования Python.
- `coverage` -- формирование отчёта об уровне покрытия исходного кода.
- `pytest` -- утилита для запуска тестов.
- `ruff` -- утилита для форматирования и проверки стиля кодирования.

Пример использования и журнал работы процессора на примере `cat`:

Пример использования для моего языка:

```shell
$ ./mv_translator.py examples/pushpop.myasm target.out
$ ./mv_machine.py target.out examples/input/pushpop.txt

```

Выводится листинг всех регистров.

- Значения всех регистров, кроме PS и CR выводятся в десятичном формате
- Значение регистра `PS` выводится в двоичном формате для удобного определения флагов, наличия запроса прерываний и тд.
- В качестве значения регистра `CR`выводятся код оператора и операнд (при наличии)
- Если в какой-то регистр записан символ, в листинге выводится его код

Также в лог выводятся события вида `INPUT symbol` и `OUTPUT symbol`

``` shell
$ cat examples/input/pushpop.txt
[(15, 'a'), (40, 'b'), (68, 'c')]
$ cat examples/cat.bf
org 0
int: word: int_handler
org 2
A: word: 10
B: word: 20
C: word: 30
in: word: 2046
out: word: 2047

int_handler: push
    load (in)
    store (out)
    pop
    iret

start: ei
    load A
    push
    load B
    push
    load C
    store (out)
    pop
    store (out)
    pop
    store (out)
    di
    hlt
$ ./mv_translator.py examples/myasm/pushpop.myasm target.out
source LoC: 29 code instr: 24
$ cat target.out
[
  {'start_addr': 12 },
  {'index': 0, 'value': 7, 'opcode': 'nop'},
  {'index': 2, 'value': 10, 'opcode': 'nop'},
  {'index': 3, 'value': 20, 'opcode': 'nop'},
  {'index': 4, 'value': 30, 'opcode': 'nop'},
  {'index': 5, 'value': 2046, 'opcode': 'nop'},
  {'index': 6, 'value': 2047, 'opcode': 'nop'},
  {'index': 7, 'opcode': 'push', 'value': 0},
  {'index': 8, 'opcode': 'load', 'operand': 5, 'value': 0, 'address': True},
  {'index': 9, 'opcode': 'store', 'operand': 6, 'value': 0, 'address': True},
  {'index': 10, 'opcode': 'pop', 'value': 0},
  {'index': 11, 'opcode': 'iret', 'value': 0},
  {'index': 12, 'opcode': 'ei', 'value': 0},
  {'index': 13, 'opcode': 'load', 'operand': 2, 'value': 0, 'address': False},
  {'index': 14, 'opcode': 'push', 'value': 0},
  {'index': 15, 'opcode': 'load', 'operand': 3, 'value': 0, 'address': False},
  {'index': 16, 'opcode': 'push', 'value': 0},
  {'index': 17, 'opcode': 'load', 'operand': 4, 'value': 0, 'address': False},
  {'index': 18, 'opcode': 'store', 'operand': 6, 'value': 0, 'address': True},
  {'index': 19, 'opcode': 'pop', 'value': 0},
  {'index': 20, 'opcode': 'store', 'operand': 6, 'value': 0, 'address': True},
  {'index': 21, 'opcode': 'pop', 'value': 0},
  {'index': 22, 'opcode': 'store', 'operand': 6, 'value': 0, 'address': True},
  {'index': 23, 'opcode': 'di', 'value': 0},
  {'index': 24, 'opcode': 'hlt', 'value': 0}]
$ ./mv_machine.py target.out examples/input/pushpop.txt
TICK:    0 | AC 0       | IP: 13   | AR: 12   | PS: 00010 | DR: 0       | SP : 2045 | mem[AR] 0       | mem[SP] : 0   | CR: ei           | main: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:    1 | AC 0       | IP: 13   | AR: 12   | PS: 10010 | DR: 0       | SP : 2045 | mem[AR] 0       | mem[SP] : 0   | CR: ei           | main: exec.f: 1 -> PS[4]


TICK:    2 | AC 0       | IP: 14   | AR: 13   | PS: 10010 | DR: 0       | SP : 2045 | mem[AR] 0       | mem[SP] : 0   | CR: load 2       | main: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:    3 | AC 0       | IP: 14   | AR: 2    | PS: 10010 | DR: 10      | SP : 2045 | mem[AR] 10      | mem[SP] : 0   | CR: load 2       | main: op.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:    4 | AC 10      | IP: 14   | AR: 2    | PS: 10000 | DR: 10      | SP : 2045 | mem[AR] 10      | mem[SP] : 0   | CR: load 2       | main: exec.f: DR -> AC


TICK:    5 | AC 10      | IP: 15   | AR: 14   | PS: 10000 | DR: 10      | SP : 2045 | mem[AR] 0       | mem[SP] : 0   | CR: push         | main: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:    6 | AC 10      | IP: 15   | AR: 2045 | PS: 10000 | DR: 10      | SP : 2044 | mem[AR] 10      | mem[SP] : 0   | CR: push         | main: exec.f: AC -> DR, SP -> AR, SP - 1 -> SP, DR -> mem[SP]


TICK:    7 | AC 10      | IP: 16   | AR: 15   | PS: 10000 | DR: 10      | SP : 2044 | mem[AR] 0       | mem[SP] : 0   | CR: load 3       | main: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:    8 | AC 10      | IP: 16   | AR: 3    | PS: 10000 | DR: 20      | SP : 2044 | mem[AR] 20      | mem[SP] : 0   | CR: load 3       | main: op.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:    9 | AC 20      | IP: 16   | AR: 3    | PS: 10000 | DR: 20      | SP : 2044 | mem[AR] 20      | mem[SP] : 0   | CR: load 3       | main: exec.f: DR -> AC


TICK:   10 | AC 20      | IP: 17   | AR: 16   | PS: 10000 | DR: 20      | SP : 2044 | mem[AR] 0       | mem[SP] : 0   | CR: push         | main: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   11 | AC 20      | IP: 17   | AR: 2044 | PS: 10000 | DR: 20      | SP : 2043 | mem[AR] 20      | mem[SP] : 0   | CR: push         | main: exec.f: AC -> DR, SP -> AR, SP - 1 -> SP, DR -> mem[SP]


TICK:   12 | AC 20      | IP: 18   | AR: 17   | PS: 10000 | DR: 20      | SP : 2043 | mem[AR] 0       | mem[SP] : 0   | CR: load 4       | main: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   13 | AC 20      | IP: 18   | AR: 4    | PS: 10000 | DR: 30      | SP : 2043 | mem[AR] 30      | mem[SP] : 0   | CR: load 4       | main: op.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:   14 | AC 30      | IP: 18   | AR: 4    | PS: 10000 | DR: 30      | SP : 2043 | mem[AR] 30      | mem[SP] : 0   | CR: load 4       | main: exec.f: DR -> AC
INPUT a


TICK:   15 | AC 30      | IP: 18   | AR: 2043 | PS: 10000 | DR: 16      | SP : 2043 | mem[AR] 0       | mem[SP] : 0   | CR: load 4       | int: 0 -> PS[3], IP -> DR, SP -> AR
TICK:   16 | AC 30      | IP: 18   | AR: 2043 | PS: 10000 | DR: 16      | SP : 2043 | mem[AR] 16      | mem[SP] : 16  | CR: load 4       | int: DR -> mem[SP]
TICK:   17 | AC 30      | IP: 18   | AR: 2042 | PS: 10000 | DR: 18      | SP : 2042 | mem[AR] 0       | mem[SP] : 0   | CR: load 4       | int: SP - 1 -> SP, 0 -> PS[4], IP -> DR, SP -> AR
TICK:   18 | AC 30      | IP: 18   | AR: 2042 | PS: 10000 | DR: 18      | SP : 2042 | mem[AR] 18      | mem[SP] : 18  | CR: load 4       | int: DR -> mem[SP]
TICK:   19 | AC 30      | IP: 18   | AR: 0    | PS: 10000 | DR: 7       | SP : 2041 | mem[AR] 7       | mem[SP] : 0   | CR: load 4       | int: SP - 1 -> SP, 0 -> AR, mem[AR] -> DR
TICK:   20 | AC 30      | IP: 7    | AR: 0    | PS: 10000 | DR: 7       | SP : 2041 | mem[AR] 7       | mem[SP] : 0   | CR: load 4       | int: DR -> IP
TICK:   21 | AC 30      | IP: 8    | AR: 7    | PS: 10000 | DR: 7       | SP : 2041 | mem[AR] 0       | mem[SP] : 0   | CR: push         | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   22 | AC 30      | IP: 8    | AR: 2041 | PS: 10000 | DR: 30      | SP : 2040 | mem[AR] 30      | mem[SP] : 0   | CR: push         | int: exec.f: AC -> DR, SP -> AR, SP - 1 -> SP, DR -> mem[SP]


TICK:   23 | AC 30      | IP: 9    | AR: 8    | PS: 10000 | DR: 30      | SP : 2040 | mem[AR] 0       | mem[SP] : 0   | CR: load 5       | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   24 | AC 30      | IP: 9    | AR: 5    | PS: 10000 | DR: 2046    | SP : 2040 | mem[AR] 2046    | mem[SP] : 0   | CR: load 5       | int: addr.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:   25 | AC 30      | IP: 9    | AR: 2046 | PS: 10000 | DR: 97      | SP : 2040 | mem[AR] 97      | mem[SP] : 0   | CR: load 5       | int: op.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:   26 | AC 97      | IP: 9    | AR: 2046 | PS: 10000 | DR: 97      | SP : 2040 | mem[AR] 97      | mem[SP] : 0   | CR: load 5       | int: exec.f: DR -> AC


TICK:   27 | AC 97      | IP: 10   | AR: 9    | PS: 10000 | DR: 97      | SP : 2040 | mem[AR] 0       | mem[SP] : 0   | CR: store 6      | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   28 | AC 97      | IP: 10   | AR: 6    | PS: 10000 | DR: 2047    | SP : 2040 | mem[AR] 2047    | mem[SP] : 0   | CR: store 6      | int: addr.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:   29 | AC 97      | IP: 10   | AR: 2047 | PS: 10000 | DR: 0       | SP : 2040 | mem[AR] 0       | mem[SP] : 0   | CR: store 6      | int: op.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
OUTPUT a
TICK:   30 | AC 97      | IP: 10   | AR: 2047 | PS: 10000 | DR: 97      | SP : 2040 | mem[AR] 97      | mem[SP] : 0   | CR: store 6      | int: exec.f: AC -> DR, DR -> mem[AR]


TICK:   31 | AC 97      | IP: 11   | AR: 10   | PS: 10000 | DR: 97      | SP : 2040 | mem[AR] 0       | mem[SP] : 0   | CR: pop          | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   32 | AC 30      | IP: 11   | AR: 2041 | PS: 10000 | DR: 30      | SP : 2041 | mem[AR] 30      | mem[SP] : 30  | CR: pop          | int: exec.f: SP + 1 -> SP, SP -> AR, mem[SP] -> DR, DR -> AC


TICK:   33 | AC 30      | IP: 12   | AR: 11   | PS: 10000 | DR: 30      | SP : 2041 | mem[AR] 0       | mem[SP] : 30  | CR: iret         | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   34 | AC 30      | IP: 12   | AR: 11   | PS: 10000 | DR: 30      | SP : 2041 | mem[AR] 0       | mem[SP] : 30  | CR: iret         | int: exec.f:  return from interrupt
TICK:   35 | AC 30      | IP: 18   | AR: 2042 | PS: 10000 | DR: 18      | SP : 2042 | mem[AR] 18      | mem[SP] : 18  | CR: iret         | int: SP + 1 -> SP, SP -> AR, mem[AR] -> DR, DR -> IP
TICK:   36 | AC 30      | IP: 18   | AR: 2043 | PS: 10000 | DR: 16      | SP : 2043 | mem[AR] 16      | mem[SP] : 16  | CR: iret         | int: SP + 1 -> SP, SP -> AR, mem[AR] -> DR, DR -> PS
TICK:   37 | AC 30      | IP: 19   | AR: 18   | PS: 10000 | DR: 16      | SP : 2043 | mem[AR] 0       | mem[SP] : 16  | CR: store 6      | main: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   38 | AC 30      | IP: 19   | AR: 6    | PS: 10000 | DR: 2047    | SP : 2043 | mem[AR] 2047    | mem[SP] : 16  | CR: store 6      | main: addr.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:   39 | AC 30      | IP: 19   | AR: 2047 | PS: 10000 | DR: 97      | SP : 2043 | mem[AR] 97      | mem[SP] : 16  | CR: store 6      | main: op.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
INPUT b
OUTPUT 30
TICK:   40 | AC 30      | IP: 19   | AR: 2047 | PS: 11000 | DR: 30      | SP : 2043 | mem[AR] 30      | mem[SP] : 16  | CR: store 6      | main: exec.f: AC -> DR, DR -> mem[AR]


TICK:   41 | AC 30      | IP: 19   | AR: 2043 | PS: 10000 | DR: 16      | SP : 2043 | mem[AR] 16      | mem[SP] : 16  | CR: store 6      | int: 0 -> PS[3], IP -> DR, SP -> AR
TICK:   42 | AC 30      | IP: 19   | AR: 2043 | PS: 10000 | DR: 16      | SP : 2043 | mem[AR] 16      | mem[SP] : 16  | CR: store 6      | int: DR -> mem[SP]
TICK:   43 | AC 30      | IP: 19   | AR: 2042 | PS: 10000 | DR: 19      | SP : 2042 | mem[AR] 18      | mem[SP] : 18  | CR: store 6      | int: SP - 1 -> SP, 0 -> PS[4], IP -> DR, SP -> AR
TICK:   44 | AC 30      | IP: 19   | AR: 2042 | PS: 10000 | DR: 19      | SP : 2042 | mem[AR] 19      | mem[SP] : 19  | CR: store 6      | int: DR -> mem[SP]
TICK:   45 | AC 30      | IP: 19   | AR: 0    | PS: 10000 | DR: 7       | SP : 2041 | mem[AR] 7       | mem[SP] : 30  | CR: store 6      | int: SP - 1 -> SP, 0 -> AR, mem[AR] -> DR
TICK:   46 | AC 30      | IP: 7    | AR: 0    | PS: 10000 | DR: 7       | SP : 2041 | mem[AR] 7       | mem[SP] : 30  | CR: store 6      | int: DR -> IP
TICK:   47 | AC 30      | IP: 8    | AR: 7    | PS: 10000 | DR: 7       | SP : 2041 | mem[AR] 0       | mem[SP] : 30  | CR: push         | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   48 | AC 30      | IP: 8    | AR: 2041 | PS: 10000 | DR: 30      | SP : 2040 | mem[AR] 30      | mem[SP] : 0   | CR: push         | int: exec.f: AC -> DR, SP -> AR, SP - 1 -> SP, DR -> mem[SP]


TICK:   49 | AC 30      | IP: 9    | AR: 8    | PS: 10000 | DR: 30      | SP : 2040 | mem[AR] 0       | mem[SP] : 0   | CR: load 5       | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   50 | AC 30      | IP: 9    | AR: 5    | PS: 10000 | DR: 2046    | SP : 2040 | mem[AR] 2046    | mem[SP] : 0   | CR: load 5       | int: addr.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:   51 | AC 30      | IP: 9    | AR: 2046 | PS: 10000 | DR: 98      | SP : 2040 | mem[AR] 98      | mem[SP] : 0   | CR: load 5       | int: op.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:   52 | AC 98      | IP: 9    | AR: 2046 | PS: 10000 | DR: 98      | SP : 2040 | mem[AR] 98      | mem[SP] : 0   | CR: load 5       | int: exec.f: DR -> AC


TICK:   53 | AC 98      | IP: 10   | AR: 9    | PS: 10000 | DR: 98      | SP : 2040 | mem[AR] 0       | mem[SP] : 0   | CR: store 6      | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   54 | AC 98      | IP: 10   | AR: 6    | PS: 10000 | DR: 2047    | SP : 2040 | mem[AR] 2047    | mem[SP] : 0   | CR: store 6      | int: addr.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:   55 | AC 98      | IP: 10   | AR: 2047 | PS: 10000 | DR: 30      | SP : 2040 | mem[AR] 30      | mem[SP] : 0   | CR: store 6      | int: op.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
OUTPUT b
TICK:   56 | AC 98      | IP: 10   | AR: 2047 | PS: 10000 | DR: 98      | SP : 2040 | mem[AR] 98      | mem[SP] : 0   | CR: store 6      | int: exec.f: AC -> DR, DR -> mem[AR]


TICK:   57 | AC 98      | IP: 11   | AR: 10   | PS: 10000 | DR: 98      | SP : 2040 | mem[AR] 0       | mem[SP] : 0   | CR: pop          | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   58 | AC 30      | IP: 11   | AR: 2041 | PS: 10000 | DR: 30      | SP : 2041 | mem[AR] 30      | mem[SP] : 30  | CR: pop          | int: exec.f: SP + 1 -> SP, SP -> AR, mem[SP] -> DR, DR -> AC


TICK:   59 | AC 30      | IP: 12   | AR: 11   | PS: 10000 | DR: 30      | SP : 2041 | mem[AR] 0       | mem[SP] : 30  | CR: iret         | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   60 | AC 30      | IP: 12   | AR: 11   | PS: 10000 | DR: 30      | SP : 2041 | mem[AR] 0       | mem[SP] : 30  | CR: iret         | int: exec.f:  return from interrupt
TICK:   61 | AC 30      | IP: 19   | AR: 2042 | PS: 10000 | DR: 19      | SP : 2042 | mem[AR] 19      | mem[SP] : 19  | CR: iret         | int: SP + 1 -> SP, SP -> AR, mem[AR] -> DR, DR -> IP
TICK:   62 | AC 30      | IP: 19   | AR: 2043 | PS: 10000 | DR: 16      | SP : 2043 | mem[AR] 16      | mem[SP] : 16  | CR: iret         | int: SP + 1 -> SP, SP -> AR, mem[AR] -> DR, DR -> PS
TICK:   63 | AC 30      | IP: 20   | AR: 19   | PS: 10000 | DR: 16      | SP : 2043 | mem[AR] 0       | mem[SP] : 16  | CR: pop          | main: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   64 | AC 20      | IP: 20   | AR: 2044 | PS: 10000 | DR: 20      | SP : 2044 | mem[AR] 20      | mem[SP] : 20  | CR: pop          | main: exec.f: SP + 1 -> SP, SP -> AR, mem[SP] -> DR, DR -> AC


TICK:   65 | AC 20      | IP: 21   | AR: 20   | PS: 10000 | DR: 20      | SP : 2044 | mem[AR] 0       | mem[SP] : 20  | CR: store 6      | main: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   66 | AC 20      | IP: 21   | AR: 6    | PS: 10000 | DR: 2047    | SP : 2044 | mem[AR] 2047    | mem[SP] : 20  | CR: store 6      | main: addr.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:   67 | AC 20      | IP: 21   | AR: 2047 | PS: 10000 | DR: 98      | SP : 2044 | mem[AR] 98      | mem[SP] : 20  | CR: store 6      | main: op.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
INPUT c
OUTPUT 20
TICK:   68 | AC 20      | IP: 21   | AR: 2047 | PS: 11000 | DR: 20      | SP : 2044 | mem[AR] 20      | mem[SP] : 20  | CR: store 6      | main: exec.f: AC -> DR, DR -> mem[AR]


TICK:   69 | AC 20      | IP: 21   | AR: 2044 | PS: 10000 | DR: 16      | SP : 2044 | mem[AR] 20      | mem[SP] : 20  | CR: store 6      | int: 0 -> PS[3], IP -> DR, SP -> AR
TICK:   70 | AC 20      | IP: 21   | AR: 2044 | PS: 10000 | DR: 16      | SP : 2044 | mem[AR] 16      | mem[SP] : 16  | CR: store 6      | int: DR -> mem[SP]
TICK:   71 | AC 20      | IP: 21   | AR: 2043 | PS: 10000 | DR: 21      | SP : 2043 | mem[AR] 16      | mem[SP] : 16  | CR: store 6      | int: SP - 1 -> SP, 0 -> PS[4], IP -> DR, SP -> AR
TICK:   72 | AC 20      | IP: 21   | AR: 2043 | PS: 10000 | DR: 21      | SP : 2043 | mem[AR] 21      | mem[SP] : 21  | CR: store 6      | int: DR -> mem[SP]
TICK:   73 | AC 20      | IP: 21   | AR: 0    | PS: 10000 | DR: 7       | SP : 2042 | mem[AR] 7       | mem[SP] : 19  | CR: store 6      | int: SP - 1 -> SP, 0 -> AR, mem[AR] -> DR
TICK:   74 | AC 20      | IP: 7    | AR: 0    | PS: 10000 | DR: 7       | SP : 2042 | mem[AR] 7       | mem[SP] : 19  | CR: store 6      | int: DR -> IP
TICK:   75 | AC 20      | IP: 8    | AR: 7    | PS: 10000 | DR: 7       | SP : 2042 | mem[AR] 0       | mem[SP] : 19  | CR: push         | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   76 | AC 20      | IP: 8    | AR: 2042 | PS: 10000 | DR: 20      | SP : 2041 | mem[AR] 20      | mem[SP] : 30  | CR: push         | int: exec.f: AC -> DR, SP -> AR, SP - 1 -> SP, DR -> mem[SP]


TICK:   77 | AC 20      | IP: 9    | AR: 8    | PS: 10000 | DR: 20      | SP : 2041 | mem[AR] 0       | mem[SP] : 30  | CR: load 5       | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   78 | AC 20      | IP: 9    | AR: 5    | PS: 10000 | DR: 2046    | SP : 2041 | mem[AR] 2046    | mem[SP] : 30  | CR: load 5       | int: addr.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:   79 | AC 20      | IP: 9    | AR: 2046 | PS: 10000 | DR: 99      | SP : 2041 | mem[AR] 99      | mem[SP] : 30  | CR: load 5       | int: op.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:   80 | AC 99      | IP: 9    | AR: 2046 | PS: 10000 | DR: 99      | SP : 2041 | mem[AR] 99      | mem[SP] : 30  | CR: load 5       | int: exec.f: DR -> AC


TICK:   81 | AC 99      | IP: 10   | AR: 9    | PS: 10000 | DR: 99      | SP : 2041 | mem[AR] 0       | mem[SP] : 30  | CR: store 6      | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   82 | AC 99      | IP: 10   | AR: 6    | PS: 10000 | DR: 2047    | SP : 2041 | mem[AR] 2047    | mem[SP] : 30  | CR: store 6      | int: addr.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:   83 | AC 99      | IP: 10   | AR: 2047 | PS: 10000 | DR: 20      | SP : 2041 | mem[AR] 20      | mem[SP] : 30  | CR: store 6      | int: op.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
OUTPUT c
TICK:   84 | AC 99      | IP: 10   | AR: 2047 | PS: 10000 | DR: 99      | SP : 2041 | mem[AR] 99      | mem[SP] : 30  | CR: store 6      | int: exec.f: AC -> DR, DR -> mem[AR]


TICK:   85 | AC 99      | IP: 11   | AR: 10   | PS: 10000 | DR: 99      | SP : 2041 | mem[AR] 0       | mem[SP] : 30  | CR: pop          | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   86 | AC 20      | IP: 11   | AR: 2042 | PS: 10000 | DR: 20      | SP : 2042 | mem[AR] 20      | mem[SP] : 20  | CR: pop          | int: exec.f: SP + 1 -> SP, SP -> AR, mem[SP] -> DR, DR -> AC


TICK:   87 | AC 20      | IP: 12   | AR: 11   | PS: 10000 | DR: 20      | SP : 2042 | mem[AR] 0       | mem[SP] : 20  | CR: iret         | int: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   88 | AC 20      | IP: 12   | AR: 11   | PS: 10000 | DR: 20      | SP : 2042 | mem[AR] 0       | mem[SP] : 20  | CR: iret         | int: exec.f:  return from interrupt
TICK:   89 | AC 20      | IP: 21   | AR: 2043 | PS: 10000 | DR: 21      | SP : 2043 | mem[AR] 21      | mem[SP] : 21  | CR: iret         | int: SP + 1 -> SP, SP -> AR, mem[AR] -> DR, DR -> IP
TICK:   90 | AC 20      | IP: 21   | AR: 2044 | PS: 10000 | DR: 16      | SP : 2044 | mem[AR] 16      | mem[SP] : 16  | CR: iret         | int: SP + 1 -> SP, SP -> AR, mem[AR] -> DR, DR -> PS
TICK:   91 | AC 20      | IP: 22   | AR: 21   | PS: 10000 | DR: 16      | SP : 2044 | mem[AR] 0       | mem[SP] : 16  | CR: pop          | main: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   92 | AC 10      | IP: 22   | AR: 2045 | PS: 10000 | DR: 10      | SP : 2045 | mem[AR] 10      | mem[SP] : 10  | CR: pop          | main: exec.f: SP + 1 -> SP, SP -> AR, mem[SP] -> DR, DR -> AC


TICK:   93 | AC 10      | IP: 23   | AR: 22   | PS: 10000 | DR: 10      | SP : 2045 | mem[AR] 0       | mem[SP] : 10  | CR: store 6      | main: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   94 | AC 10      | IP: 23   | AR: 6    | PS: 10000 | DR: 2047    | SP : 2045 | mem[AR] 2047    | mem[SP] : 10  | CR: store 6      | main: addr.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
TICK:   95 | AC 10      | IP: 23   | AR: 2047 | PS: 10000 | DR: 99      | SP : 2045 | mem[AR] 99      | mem[SP] : 10  | CR: store 6      | main: op.f: CR[operand] -> DR, DR -> AR, mem[AR] -> DR
OUTPUT 10
TICK:   96 | AC 10      | IP: 23   | AR: 2047 | PS: 10000 | DR: 10      | SP : 2045 | mem[AR] 10      | mem[SP] : 10  | CR: store 6      | main: exec.f: AC -> DR, DR -> mem[AR]


TICK:   97 | AC 10      | IP: 24   | AR: 23   | PS: 10000 | DR: 10      | SP : 2045 | mem[AR] 0       | mem[SP] : 10  | CR: di           | main: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:   98 | AC 10      | IP: 24   | AR: 23   | PS: 00000 | DR: 10      | SP : 2045 | mem[AR] 0       | mem[SP] : 10  | CR: di           | main: exec.f: 0 -> PS[4]


TICK:   99 | AC 10      | IP: 25   | AR: 24   | PS: 00000 | DR: 10      | SP : 2045 | mem[AR] 0       | mem[SP] : 10  | CR: hlt          | main: instr.f: IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR
TICK:  100 | AC 10      | IP: 25   | AR: 24   | PS: 00000 | DR: 10      | SP : 2045 | mem[AR] 0       | mem[SP] : 10  | CR: hlt          | main: exec.f: end on simulation
Output: ['a', 30, 'b', 20, 'c', 10]
Instruction number: 24
Ticks: 100
```

Пример проверки исходного кода:

``` shell
$ poetry run pytest . -v --update-goldens
============================================= test session starts ==============================================
platform linux -- Python 3.11.5, pytest-7.4.3, pluggy-1.3.0 -- /home/kristina/.cache/pypoetry/virtualenvs/myavasm-ku2xNcr2-py3.11/bin/python
cachedir: .pytest_cache
rootdir: /home/kristina/PycharmProjects/csa-lab-3-myavasm
configfile: pyproject.toml
plugins: golden-0.2.2
collected 5 items                                                                                              

mv_integration_test.py::test_translator_and_machine[golden/prob2.yml] PASSED                             [ 20%]
mv_integration_test.py::test_translator_and_machine[golden/hellouser.yml] PASSED                         [ 40%]
mv_integration_test.py::test_translator_and_machine[golden/hi.yml] PASSED                                [ 60%]
mv_integration_test.py::test_translator_and_machine[golden/cat.yml] PASSED                               [ 80%]
mv_integration_test.py::test_translator_and_machine[golden/poppush.yml] PASSED                           [100%]

============================================== 5 passed in 0.97s ===============================================
$ poetry run ruff format .
5 files left unchanged

```

```text
| ФИО                            | алг              | LoC | code байт | code инстр. | инстр. | такт. | вариант |
| Кравцова Кристина Владимировна | hello            | 20  | -         | 29          | 121    | 364   | asm | acc | neum | hw | tick | struct | trap | mem | pstr | prob2 | spi     |
| Кравцова Кристина Владимировна | cat              | 25  | -         | 20          | 66     | 256   | asm | acc | neum | hw | tick | struct | trap | mem | pstr | prob2 | spi     |
| Кравцова Кристина Владимировна | hello_username   | 106 | -         | 111         | 501    | 1606  | asm | acc | neum | hw | tick | struct | trap | mem | pstr | prob2 | spi     |
| Кравцова Кристина Владимировна | prob2            | 29  | -         | 25          | 370    | 1071  | asm | acc | neum | hw | tick | struct | trap | mem | pstr | prob2 | spi     |


```
